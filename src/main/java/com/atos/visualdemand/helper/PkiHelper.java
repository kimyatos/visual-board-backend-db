package com.atos.visualdemand.helper;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.atos.visualdemand.exception.InvalidPkiDataException;

/**
 * Helper class for reading PKI data
 * 
 * @author a630641
 *
 */
public class PkiHelper {
	private static final String MISSING_REQUEST_ERROR_MESSAGE = "MUST specify http request";
	public static final String PKI_HEADER = "X-SSL-Client-S-DN";
	public static final String DAS_KEY = "serialNumber";

	private HttpServletRequest request;
	private Map<String, String> props;

	public PkiHelper() {
		throw new AssertionError(MISSING_REQUEST_ERROR_MESSAGE);
	}

	/**
	 * PkiHelper constructor
	 * 
	 * @param request
	 *            Mandatory param
	 * @throws AssertionError
	 *             if param is not present
	 * @author a630641
	 */
	public PkiHelper(HttpServletRequest request) {
		if (request == null) {
			throw new AssertionError(MISSING_REQUEST_ERROR_MESSAGE);
		}
		this.request = request;
	}

	/**
	 * Finds the PKI data from the HTTP header
	 * 
	 * @return All data from the PKI
	 * @throws InvalidPkiDataException
	 *             When PKI header is not present in HTTP headers
	 * @author a630641
	 */
	public String findPkiData() {
		String pkiData = request.getHeader(PKI_HEADER);
		if (pkiData == null) {
			throw new InvalidPkiDataException("PKI data was not sent, but was expected");
		}
		return pkiData;
	}

	/**
	 * Returns a property from the PKI
	 * 
	 * @param propertyKey
	 *            The key of the property
	 * @return Value of the property
	 * @throws InvalidPkiDataException
	 *             When <b>propertyKey</b> doesn't exists
	 * @author a630641
	 */
	public String getPropertyValue(String propertyKey) {
		if (props == null) {
			parsePropertiesGeneration();
		}
		String value = props.get(propertyKey);
		if (value == null) {
			throw new InvalidPkiDataException("could not get property " + value + " from PKI, no such property");
		}
		return value;
	}

	private void parsePropertiesGeneration() {
		props = new HashMap<>();
		String pkiData = findPkiData();
		String[] split1;
		if(pkiData.indexOf("/serialNumber") != -1){
			split1 = pkiData.split("/");
		}else if(pkiData.indexOf(",serialNumber") != -1){
			split1 = pkiData.split(",");
		}else{
			throw new InvalidPkiDataException("Pki separator should be , or /, but unexpected content: " + pkiData);
		}
		
		for (int i = 0; i < split1.length; i++) {
			if ("".equals(split1[i])) {
				continue;
			}
			String[] split2 = split1[i].split("=");
			props.put(split2[0], split2[1]);
		}
	}
}
