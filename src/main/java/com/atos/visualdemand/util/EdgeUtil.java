/**
 * 
 */
package com.atos.visualdemand.util;

import com.atos.visualdemand.model.Edge;

/**
 * Methods for doing things with edges <br>
 * <ul>
 * <li>clearProxies: will remove proxies from target edge(s)</li>
 * </ul>
 * 
 * @author a630641
 */
public abstract class EdgeUtil {

	private EdgeUtil() {
		throw new AssertionError("Can't instantiate an Util class!");
	}

	public static void clearProxies(Edge edge) {
		edge.withNullSourceAndTarget();
	}

	public static void clearProxies(Iterable<Edge> edges) {
		edges.forEach(current -> clearProxies(current));
	}
}
