package com.atos.visualdemand;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.EnvironmentAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.security.oauth2.provider.token.TokenEnhancerChain;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.InMemoryTokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import com.atos.visualdemand.exception.VisualDemandCommonException;
import com.atos.visualdemand.helper.PkiHelper;
import com.atos.visualdemand.model.User;
import com.atos.visualdemand.security.CustomAuthenticationEntryPoint;
import com.atos.visualdemand.security.CustomLogoutSuccessHandler;
import com.atos.visualdemand.service.UserService;

@Configuration
@Profile("production")
public class OAuth2Configuration {

	protected static class CustomTokenEnhancer implements TokenEnhancer {

		@Autowired
		private HttpServletRequest request;

		@Autowired
		private UserService userService;

		@Override
		public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {
			PkiHelper helper = new PkiHelper(request);
			User user = userService.findByDas(helper.getPropertyValue(PkiHelper.DAS_KEY));
			if (user == null) {
				throw new UsernameNotFoundException("User doesn't exists in database");
			}
			Map<String, Object> additionalInfo = new HashMap<>();
			additionalInfo.put("userId", user.getId());
			additionalInfo.put("userDas", user.getDas());
			additionalInfo.put("fullName", user.getFullName());
			((DefaultOAuth2AccessToken) accessToken).setAdditionalInformation(additionalInfo);
			return accessToken;
		}
	}

	@Configuration
	@EnableResourceServer
	@Profile("production")
	protected static class ResourceServerConfiguration extends ResourceServerConfigurerAdapter {

		@Autowired
		private CustomAuthenticationEntryPoint customAuthenticationEntryPoint;

		@Autowired
		private CustomLogoutSuccessHandler customLogoutSuccessHandler;

		@Bean
		CorsConfigurationSource corsConfigurationSource() {
			CorsConfiguration configuration = new org.springframework.web.cors.CorsConfiguration();
			configuration.setAllowedOrigins(Arrays.asList("http://127.0.0.1:4200", "http://localhost:4200",
					"http://192.168.99.100:4200", "https://localhost", "https://docker.local",
					"http://admin.docker.local", "https://admin.localhost", "http://admin.localhost",
					"http://localhost", "http://docker.local", "https://admin.docker.local", 
					"https://admin.visualdemand.com", "http://visualdemand.com"));
			configuration.setAllowedMethods(Arrays.asList("GET", "POST", "DELETE"));
			UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
			source.registerCorsConfiguration("/**", configuration);
			return source;
		}

		@Override
		public void configure(HttpSecurity http) throws Exception {

			http.exceptionHandling().authenticationEntryPoint(customAuthenticationEntryPoint).and().logout()
					.logoutUrl("/oauth/logout").logoutSuccessHandler(customLogoutSuccessHandler).and().cors().and()
					.csrf().requireCsrfProtectionMatcher(new AntPathRequestMatcher("/oauth/authorize")).disable()
					.headers().frameOptions().disable().and().authorizeRequests()
					.antMatchers(HttpMethod.OPTIONS, "/oauth/login").permitAll().antMatchers("/hello/").permitAll()
					.antMatchers("/secure/**").authenticated().and().sessionManagement()
					.sessionCreationPolicy(SessionCreationPolicy.STATELESS);

		}

	}

	@Configuration
	@EnableAuthorizationServer
	@Profile("production")
	protected static class AuthorizationServerConfiguration extends AuthorizationServerConfigurerAdapter
			implements EnvironmentAware {

		private static final String PROP_CLIENTID = "visualDemand";
		private static final String PROP_SECRET = "secret";
		private static final Integer PROP_TOKEN_VALIDITY_SECONDS = 1800;

		@Autowired
		@Qualifier("authenticationManagerBean")
		private AuthenticationManager authenticationManager;

		@Value("${jwt.secret}")
		private String secret;

		@Bean
		public JwtAccessTokenConverter accessTokenConverter() {
			if (secret == null) {
				throw new VisualDemandCommonException("JWT secret can't be null, please define property jwt.secret");
			}
			JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
			converter.setSigningKey(secret);
			return converter;
		}

		@Bean
		public TokenStore tokenStore() {
			return new InMemoryTokenStore();
		}

		@Bean
		public TokenEnhancer tokenEnhancer() {
			return new CustomTokenEnhancer();
		}

		@Override
		public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
			TokenEnhancerChain tokenEnhancerChain = new TokenEnhancerChain();
			tokenEnhancerChain.setTokenEnhancers(Arrays.asList(tokenEnhancer(), accessTokenConverter()));

			endpoints.tokenStore(tokenStore()).authenticationManager(authenticationManager)
					.tokenEnhancer(tokenEnhancerChain);
		}

		@Override
		public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
			clients.inMemory().withClient(PROP_CLIENTID).scopes("read", "write")
					.authorizedGrantTypes("password", "refresh_token").secret(PROP_SECRET)
					.accessTokenValiditySeconds(PROP_TOKEN_VALIDITY_SECONDS);
		}

		@Override
		public void setEnvironment(Environment arg0) {
		}

	}

}
