package com.atos.visualdemand.pojo;

import com.atos.visualdemand.model.Edge;
import com.atos.visualdemand.model.Employee;
import com.atos.visualdemand.model.Request;

/**
 * This pojo is used to send all information at once to the frontend <br>
 * It will contain
 * <ul>
 * <li>Employee nodes</li>
 * <li>Request nodes</li>
 * <li>Edges</li>
 * 
 * @author a630641
 *
 */
public class NodesPojo {
	private Iterable<Employee> employees;
	private Iterable<Request> requests;
	private Iterable<Edge> edges;

	public Iterable<Employee> getEmployees() {
		return employees;
	}

	public void setEmployees(Iterable<Employee> employees) {
		this.employees = employees;
	}

	public Iterable<Request> getRequests() {
		return requests;
	}

	public void setRequests(Iterable<Request> requests) {
		this.requests = requests;
	}

	public Iterable<Edge> getEdges() {
		return edges;
	}

	public void setEdges(Iterable<Edge> edges) {
		this.edges = edges;
	}

}
