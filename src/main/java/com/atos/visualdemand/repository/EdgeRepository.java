package com.atos.visualdemand.repository;

import org.springframework.data.repository.CrudRepository;

import com.atos.visualdemand.model.Edge;
import com.atos.visualdemand.model.Employee;
import com.atos.visualdemand.model.Request;

public interface EdgeRepository extends CrudRepository<Edge, Long> {

	void deleteBySource(Employee employee);

	void deleteByTarget(Request request);

	void deleteBySourceAndTarget(Employee employee, Request request);
}
