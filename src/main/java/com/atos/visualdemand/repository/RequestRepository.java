package com.atos.visualdemand.repository;

import org.springframework.data.repository.CrudRepository;

import com.atos.visualdemand.model.Request;

public interface RequestRepository extends CrudRepository<Request, Long> {
}
