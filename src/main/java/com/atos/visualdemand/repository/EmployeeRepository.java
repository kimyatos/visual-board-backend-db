package com.atos.visualdemand.repository;

import org.springframework.data.repository.CrudRepository;

import com.atos.visualdemand.model.Employee;

public interface EmployeeRepository extends CrudRepository<Employee, Long> {

}
