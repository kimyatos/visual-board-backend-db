package com.atos.visualdemand.repository;

import org.springframework.data.repository.CrudRepository;

import com.atos.visualdemand.model.DeepBox;

public interface DeepBoxRepository extends CrudRepository<DeepBox, Long> {
}
