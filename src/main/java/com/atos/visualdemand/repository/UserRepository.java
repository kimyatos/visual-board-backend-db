package com.atos.visualdemand.repository;

import org.springframework.data.repository.CrudRepository;

import com.atos.visualdemand.model.User;

public interface UserRepository extends CrudRepository<User, Long> {
	public User findByDas(String das);
}
