package com.atos.visualdemand.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import com.atos.visualdemand.model.User;
import com.atos.visualdemand.repository.UserRepository;

@Service
public class UserService implements AbstractCrudService<User> {

	@Autowired
	private UserRepository userRepository;

	public User findByDas(String das) {
		return userRepository.findByDas(das);
	}

	@Override
	public CrudRepository<User, Long> getRepository() {
		return userRepository;
	}

}
