package com.atos.visualdemand.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.atos.visualdemand.exception.EdgeCrudException;
import com.atos.visualdemand.model.Edge;
import com.atos.visualdemand.model.Employee;
import com.atos.visualdemand.model.Request;
import com.atos.visualdemand.repository.EdgeRepository;

@Service
public class EdgeService implements AbstractCrudService<Edge> {

	@Autowired
	private EdgeRepository edgeRepository;

	@Autowired
	private EmployeeService employeeService;

	@Autowired
	private RequestService requestService;

	@Override
	public CrudRepository<Edge, Long> getRepository() {
		return edgeRepository;
	}

	/**
	 * Before saving will check if source and target ares defined <br>
	 * A In case these properties are not, will <b>fetch from db</b> by IDs
	 * 
	 * @param edge
	 * @author a630641
	 */
	@Override
	public Edge saveOrUpdate(Edge edge) {
		putSourceIfUndefined(edge);
		putTargetIfUndefined(edge);
		checkRelationshipIsNotNull(edge);
		return AbstractCrudService.super.saveOrUpdate(edge);
	}

	/**
	 * Deletes by source and target <b>(used to remove one relation)</b>
	 * 
	 * @param sourceId
	 *            id of the <b>employee</b>
	 * @param targetid
	 *            id of the <b>request</b>
	 * @author a630641
	 */
	@Transactional
	public void deleteBySourceAndTarget(Long sourceId, Long targetId) {
		Employee employee = new Employee();
		Request request = new Request();
		employee.setId(sourceId);
		request.setId(targetId);
		edgeRepository.deleteBySourceAndTarget(employee, request);
	}

	private void putSourceIfUndefined(Edge edge) {
		if (edge.getSource() == null && edge.getSourceId() != null) {
			edge.setSource(employeeService.findById(edge.getSourceId()));
		}
	}

	private void putTargetIfUndefined(Edge edge) {
		if (edge.getTarget() == null && edge.getTargetId() != null) {
			edge.setTarget(requestService.findById(edge.getTargetId()));
		}
	}

	/**
	 * 
	 * @param edge
	 * @throws EdgeCrudException
	 *             when source or target is null
	 * @author a630641
	 */
	private void checkRelationshipIsNotNull(Edge edge) {
		if (edge.getSource() == null || edge.getTarget() == null) {
			throw new EdgeCrudException(
					"Can't save edge, when source employee or target request (or even both) are null. Maybe specified id could not be found in the database?");
		}
	}
}
