package com.atos.visualdemand.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.atos.visualdemand.model.Request;
import com.atos.visualdemand.repository.RequestRepository;

@Service
public class RequestService implements AbstractCrudService<Request> {

	@Autowired
	private RequestRepository requestRepository;

	@Override
	public CrudRepository<Request, Long> getRepository() {
		return requestRepository;
	}

	@Override
	@Transactional
	public void delete(Long id) {
		this.requestRepository.delete(id);
	}

}
