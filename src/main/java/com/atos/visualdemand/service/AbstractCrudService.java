package com.atos.visualdemand.service;

import org.springframework.data.repository.CrudRepository;

import com.atos.visualdemand.exception.EntityNotFoundException;

public interface AbstractCrudService<E> {
	public abstract CrudRepository<E, Long> getRepository();

	/**
	 * Will save or update (if id is <b>not null</b>) <br>
	 * <b>NOTICE: Will NOT do a flush </b>
	 * 
	 * @param entity
	 *            Entity to be saved to the database
	 * @return
	 * @author a630641
	 */
	public default E saveOrUpdate(E entity) {
		return getRepository().save(entity);
	}

	/**
	 * Returns a list containing all items
	 * 
	 * @return
	 */
	public default Iterable<E> findAll() {
		return getRepository().findAll();
	}

	public default E findById(Long id) {
		return getRepository().findOne(id);
	}

	/**
	 * Tries to find the entity, will throw exception if not found
	 * 
	 * @param id
	 * @return
	 * @throws EntityNotFoundException
	 *             When id doesn't exists in the database
	 * @author a630641
	 */
	public default E findByIdOrDie(Long id) {
		E entity = getRepository().findOne(id);
		if (entity == null) {
			throw new EntityNotFoundException("No such entity in " + getRepository().getClass().getName());
		}
		return entity;
	}

	public default void delete(Long id) {
		getRepository().delete(id);
	}

}