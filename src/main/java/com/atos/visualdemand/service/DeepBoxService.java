package com.atos.visualdemand.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.atos.visualdemand.model.DeepBox;
import com.atos.visualdemand.repository.DeepBoxRepository;

@Service
public class DeepBoxService {

	@Autowired
	private DeepBoxRepository deepBoxRepository;
	
	public DeepBox get() {
		return this.deepBoxRepository.findAll().iterator().next();
	}
	
	public DeepBox update(String html) {
		DeepBox deepBox =  this.deepBoxRepository.findAll().iterator().next();
		deepBox.setHtml(html);
		return this.deepBoxRepository.save(deepBox);
	}

}
