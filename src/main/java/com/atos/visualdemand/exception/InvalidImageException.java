package com.atos.visualdemand.exception;

public class InvalidImageException extends VisualDemandCommonException {
	private static final long serialVersionUID = 2865851590598054362L;

	public InvalidImageException(String message) {
		super(message);
	}

	public InvalidImageException(String message, Exception e) {
		super(message, e);
	}

}
