package com.atos.visualdemand.exception;

public class EdgeCrudException extends VisualDemandCommonException {
	private static final long serialVersionUID = 4852980283235795348L;

	public EdgeCrudException(Exception e) {
		super(e);
	}

	public EdgeCrudException(String message) {
		super(message);
	}

}
