package com.atos.visualdemand.exception;

public class InvalidPkiDataException extends VisualDemandCommonException {
	private static final long serialVersionUID = -2004273171793874981L;

	public InvalidPkiDataException(String message) {
		super(message);
	}

}
