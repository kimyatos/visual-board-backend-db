package com.atos.visualdemand.exception;

public class VisualDemandCommonException extends RuntimeException {
	private static final long serialVersionUID = -2987368843435797729L;

	public VisualDemandCommonException(Exception e) {
		super(e);
	}

	public VisualDemandCommonException(String message, Exception e) {
		super(message, e);
	}

	public VisualDemandCommonException(String message) {
		super(message);
	}
}
