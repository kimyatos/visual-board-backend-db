/**
 * 
 */
package com.atos.visualdemand.exception;

/**
 * @author a630641
 *
 */
public class UnexpectedRaceConditionException extends VisualDemandCommonException {
	private static final long serialVersionUID = -455400535693635838L;

	/**
	 * @param message
	 * @author a630641
	 */
	public UnexpectedRaceConditionException(String message) {
		super(message);
	}

}
