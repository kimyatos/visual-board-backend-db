package com.atos.visualdemand.exception;

public class EntityNotFoundException extends VisualDemandCommonException {
	private static final long serialVersionUID = 1918174179217796112L;

	public EntityNotFoundException(String message) {
		super(message);
	}

}
