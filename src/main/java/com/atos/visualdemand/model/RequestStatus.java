package com.atos.visualdemand.model;

public enum RequestStatus {
	INTERESADOS, EN_CURSO, INCUBADORA, CONFIRMADAS, DESASIGNEZONE
}
