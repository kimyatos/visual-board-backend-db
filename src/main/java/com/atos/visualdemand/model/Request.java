package com.atos.visualdemand.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "request")
public class Request extends AbstractNode {

	@Column(nullable = false)
	private String text;

	private int juniorProgrammers;

	private int seniorProgrammers;

	private boolean formationPlan;

	private boolean technicalSheet;

	private String urlFormationPlan;

	private String urlTechnicalSheet;

	@Column(nullable = false)
	private Date startingDate;

	private Date finishingDate;;

	@OneToMany(mappedBy = "target", cascade = CascadeType.REMOVE, fetch = FetchType.LAZY, targetEntity = Edge.class)
	private List<Edge> edges;

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public int getJuniorProgrammers() {
		return juniorProgrammers;
	}

	public void setJuniorProgrammers(int juniorProgrammers) {
		this.juniorProgrammers = juniorProgrammers;
	}

	public int getSeniorProgrammers() {
		return seniorProgrammers;
	}

	public void setSeniorProgrammers(int seniorProgrammers) {
		this.seniorProgrammers = seniorProgrammers;
	}

	public boolean isFormationPlan() {
		return formationPlan;
	}

	public void setFormationPlan(boolean formationPlan) {
		this.formationPlan = formationPlan;
	}

	public boolean isTechnicalSheet() {
		return technicalSheet;
	}

	public void setTechnicalSheet(boolean technicalSheet) {
		this.technicalSheet = technicalSheet;
	}

	public String getUrlFormationPlan() {
		return urlFormationPlan;
	}

	public void setUrlFormationPlan(String urlFormationPlan) {
		this.urlFormationPlan = urlFormationPlan;
	}

	public String getUrlTechnicalSheet() {
		return urlTechnicalSheet;
	}

	public void setUrlTechnicalSheet(String urlTechnicalSheet) {
		this.urlTechnicalSheet = urlTechnicalSheet;
	}

	public Date getStartingDate() {
		return startingDate;
	}

	public void setStartingDate(Date startingDate) {
		this.startingDate = startingDate;
	}

	public Date getFinishingDate() {
		return finishingDate;
	}

	public void setFinishingDate(Date finishingDate) {
		this.finishingDate = finishingDate;
	}

	public List<Edge> getEdges() {
		return edges;
	}

	public void setEdges(List<Edge> edges) {
		this.edges = edges;
	}

}
