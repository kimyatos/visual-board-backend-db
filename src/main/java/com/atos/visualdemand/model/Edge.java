package com.atos.visualdemand.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "edge")
public class Edge {
	@Id
	@GeneratedValue
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "SOURCE", nullable = false)
	private Employee source;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "TARGET", nullable = false)
	private Request target;

	@Transient
	private Long sourceId;

	@Transient
	private Long targetId;

	/**
	 * Will initialize the transient properties <br>
	 * So will put the source.id in sourceId and the target.id in targetId <br>
	 * 
	 * @author a630641
	 * @return this instance
	 */
	public Edge withIdsDefined() {
		if (this.source != null) {
			this.sourceId = this.source.getId();
		}
		if (this.target != null) {
			this.targetId = this.target.getId();
		}
		return this;
	}

	/**
	 * Sets the source and the target to null <br>
	 * Ideal before jsoning and sending back to the frontend
	 * 
	 * @return this instance
	 */
	public Edge withNullSourceAndTarget() {
		source = null;
		target = null;
		return this;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Employee getSource() {
		return source;
	}

	public void setSource(Employee source) {
		this.source = source;
	}

	public Request getTarget() {
		return target;
	}

	public void setTarget(Request target) {
		this.target = target;
	}

	public Long getSourceId() {
		return sourceId;
	}

	public void setSourceId(Long sourceId) {
		this.sourceId = sourceId;
	}

	public Long getTargetId() {
		return targetId;
	}

	public void setTargetId(Long targetId) {
		this.targetId = targetId;
	}
}
