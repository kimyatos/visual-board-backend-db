package com.atos.visualdemand.model;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

/**
 * This class contains the common position fields, used by both Request, and Employee classes
 * @author a630641
 *
 */
@MappedSuperclass
public abstract class NodePosition {
	@Column(nullable = false)
	private Integer x;
	
	@Column(nullable = false)
	private Integer y;
	
	public Integer getX() {
		return x;
	}
	public void setX(Integer x) {
		this.x = x;
	}
	public Integer getY() {
		return y;
	}
	public void setY(Integer y) {
		this.y = y;
	}
	
	
}
