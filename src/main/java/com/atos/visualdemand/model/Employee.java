package com.atos.visualdemand.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "employee")
public class Employee extends AbstractNode {

	@Column(nullable = false)
	private String				name;

	/**
	 * This field has a length because in testing env, the target type is a
	 * varchar, use with caution!
	 * 
	 * @author a630641
	 */
	@Column(length = 999999)
	private byte[]				image;

	@Transient
	private String				base64Image;

	@Transient
	private String				imageUrl;

	@Enumerated(EnumType.STRING)
	private ProgrammerSkillEnum	developerSkill;

	@OneToMany(mappedBy = "source", cascade = CascadeType.REMOVE, fetch = FetchType.LAZY, targetEntity = Edge.class)
	private List<Edge>			edges;

	/**
	 * Tells if we have to render it in the board
	 */
	@Column
	private boolean				isInBacklog	= true;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public byte[] getImage() {
		return image;
	}

	public void setImage(byte[] image) {
		this.image = image;
	}

	public String getBase64Image() {
		return base64Image;
	}

	public void setBase64Image(String base64Image) {
		this.base64Image = base64Image;
	}

	/**
	 * Path to the image
	 * 
	 * @author a630641
	 */
	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public ProgrammerSkillEnum getDeveloperSkill() {
		return developerSkill;
	}

	public void setDeveloperSkill(ProgrammerSkillEnum developerSkill) {
		this.developerSkill = developerSkill;
	}

	public List<Edge> getEdges() {
		return edges;
	}

	public void setEdges(List<Edge> edges) {
		this.edges = edges;
	}

	public boolean isInBacklog() {
		return isInBacklog;
	}

	public void setInBacklog(boolean isInBacklog) {
		this.isInBacklog = isInBacklog;
	}
}
