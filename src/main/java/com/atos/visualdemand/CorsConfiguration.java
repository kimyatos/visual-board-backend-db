package com.atos.visualdemand;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
public class CorsConfiguration {
	@Bean
	public WebMvcConfigurer corsConfigurer() {
		return new WebMvcConfigurerAdapter() {
			@Override
			public void addCorsMappings(CorsRegistry registry) {
				registry.addMapping("/**")
						.allowedOrigins("http://127.0.0.1:4200", "http://localhost:4200", "http://192.168.99.100:4200",
								"https://localhost", "https://docker.local", "http://admin.docker.local",
								"https://admin.localhost", "http://admin.localhost", "http://localhost",
								"http://docker.local", "https://admin.docker.local","http://admin.localhost:4200", 
								"https://admin.visualdemand.com", "http://visualdemand.com")
						.allowedMethods("GET", "POST", "DELETE");
			}
		};
	}
}
