package com.atos.visualdemand.controller;

import java.util.Base64;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.atos.visualdemand.model.Employee;
import com.atos.visualdemand.service.EmployeeService;

/**
 * Holds Crud operations related to {@link Employee} entity
 * 
 * @author a630641
 *
 */
@RestController
@RequestMapping(value = "/employee")
public class EmployeeController {

	@Autowired
	private EmployeeService employeeService;

	@RequestMapping(value = "/save", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("isAuthenticated()")
	public Employee save(@RequestBody Employee employee) {
		return employeeService.saveOrUpdate(employee);
	}

	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
	@PreAuthorize("isAuthenticated()")
	public void delete(@PathVariable(value = "id") Long id) {
		employeeService.delete(id);
	}

	@ResponseBody
	@RequestMapping(value = "/image/default", method = RequestMethod.GET, produces = MediaType.IMAGE_JPEG_VALUE)
	public byte[] defaultImage() {
		return Base64.getDecoder().decode((EmployeeService.DEFAULT_IMAGE));
	}

	@ResponseBody
	@RequestMapping(value = "/image/{employeeId}", method = RequestMethod.GET, produces = MediaType.IMAGE_JPEG_VALUE)
	public byte[] image(@PathVariable(value = "employeeId") Long employeeId, HttpServletResponse response) {
		byte[] image = employeeService.findByIdOrDie(employeeId).getImage();
		if (image == null) {
			response.setStatus(404);
		}
		return image;

	}
}
