package com.atos.visualdemand.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.atos.visualdemand.service.DeepBoxService;

@RestController
@RequestMapping(value = "/deepbox")
public class DeepBoxController {

	@Autowired
	private DeepBoxService deepBoxService;

	@RequestMapping(value = "/get", method = RequestMethod.GET)
	public String get() {
		return this.deepBoxService.get().getHtml();
	}

	@RequestMapping(value = "/put", method = RequestMethod.POST)
	public String put(@RequestParam(value = "html") String html) {
		return this.deepBoxService.update(html).getHtml();
	}
}
