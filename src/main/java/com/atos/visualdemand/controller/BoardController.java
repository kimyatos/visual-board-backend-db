package com.atos.visualdemand.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.atos.visualdemand.model.Edge;
import com.atos.visualdemand.model.Employee;
import com.atos.visualdemand.model.Request;
import com.atos.visualdemand.pojo.NodesPojo;
import com.atos.visualdemand.service.EdgeService;
import com.atos.visualdemand.service.EmployeeService;
import com.atos.visualdemand.service.RequestService;

@RestController
@RequestMapping(value = "/board")
public class BoardController {

	@Autowired
	private EmployeeService employeeService;

	@Autowired
	private RequestService requestService;

	@Autowired
	private EdgeService edgesService;

	@RequestMapping(value = "/get", method = RequestMethod.GET)
	public NodesPojo findAll() {
		NodesPojo retVal = new NodesPojo();
		Iterable<Employee> employee = findEmployeesWithoutImage();
		Iterable<Request> request = requestService.findAll();
		Iterable<Edge> edges = edgesService.findAll();

		edges.forEach(current -> current.withIdsDefined().withNullSourceAndTarget());
		employee.forEach(current -> current.setEdges(null));
		request.forEach(current -> current.setEdges(null));

		retVal.setEmployees(employee);
		retVal.setRequests(request);
		retVal.setEdges(edges);

		return retVal;
	}

	private Iterable<Employee> findEmployeesWithoutImage() {
		Iterable<Employee> employees = employeeService.findAll();
		employees.forEach(current -> current.setImage(null));
		return employees;
	}
}
