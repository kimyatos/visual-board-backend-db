package com.atos.visualdemand.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.atos.visualdemand.exception.VisualDemandCommonException;
import com.atos.visualdemand.pojo.ExceptionResponse;

@ControllerAdvice
public class VisualDemandExceptionHandler {

	private static final Logger LOG = LoggerFactory.getLogger(VisualDemandExceptionHandler.class);

	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ExceptionHandler(VisualDemandCommonException.class)
	@ResponseBody
	public ExceptionResponse handleApplicationException(Exception ex) {
		LOG.debug("Application error", ex);
		return new ExceptionResponse(ex.getMessage());
	}

	/**
	 * Instead of having level error, should have level info, as this exception
	 * is a common expected one!
	 * 
	 * @param ex
	 * @return
	 * @author a630641
	 */
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ExceptionHandler(AccessDeniedException.class)
	@ResponseBody
	public ExceptionResponse handleAccessDeniedException(Exception ex) {
		LOG.info("Access denied!", ex);
		return new ExceptionResponse(ex.getMessage());
	}

	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ExceptionHandler(Exception.class)
	@ResponseBody
	public ExceptionResponse handleNoMoreCustomWords(Exception ex) {
		LOG.error("Unexpected error", ex);
		return new ExceptionResponse(ex.getMessage());
	}
}
