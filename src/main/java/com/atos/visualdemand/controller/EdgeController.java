package com.atos.visualdemand.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.atos.visualdemand.model.Edge;
import com.atos.visualdemand.service.EdgeService;

@RestController
@RequestMapping(value = "/edge")
public class EdgeController {

	@Autowired
	private EdgeService edgeService;

	@RequestMapping(value = "/save", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("isAuthenticated()")
	public Edge save(@RequestBody Edge edge) {
		Edge savedEdge = edgeService.saveOrUpdate(edge);
		savedEdge.setSource(null);
		savedEdge.setTarget(null);
		return savedEdge;
	}

	@RequestMapping(value = "/delete", method = RequestMethod.DELETE)
	@PreAuthorize("isAuthenticated()")
	public Boolean delete(@RequestParam("sourceId") Long sourceId, @RequestParam("targetId") Long targetId) {
		edgeService.deleteBySourceAndTarget(sourceId, targetId);
		return true;
	}
}
