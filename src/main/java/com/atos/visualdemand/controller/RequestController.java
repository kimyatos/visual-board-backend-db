package com.atos.visualdemand.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.atos.visualdemand.model.Request;
import com.atos.visualdemand.service.RequestService;

/**
 * Holds all operations for a request card.
 * 
 * @author A643239
 *
 */
@RestController
@RequestMapping(value = "/request")
public class RequestController {

	@Autowired
	private RequestService requestService;

	@RequestMapping(value = "/save", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("isAuthenticated()")
	public Request save(@RequestBody Request request) {
		return this.requestService.saveOrUpdate(request);
	}

	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
	@PreAuthorize("isAuthenticated()")
	public void delete(@PathVariable(value = "id") Long id) {
		requestService.delete(id);
	}
}
