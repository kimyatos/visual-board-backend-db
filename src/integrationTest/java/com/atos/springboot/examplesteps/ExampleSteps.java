package com.atos.springboot.examplesteps;

import java.text.ParseException;

import org.jbehave.core.annotations.BeforeStories;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Named;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.springframework.stereotype.Component;

@Component
public class ExampleSteps {

	// @Autowired
	// private WebApplicationContext ctx

	// private MockMvc mockMvc

	@BeforeStories
	public void setUp() throws ParseException {
		/*
		 * this.mockMvc = MockMvcBuilders.webAppContextSetup(this.ctx).apply(
		 * SecurityMockMvcConfigurers.springSecurity()) .build()
		 */
	}

	@Given("a user with username $username has sign up")
	public void userSignsUp(@Named("username") String username) {
		// It's a Skeleton example
	}

	@When("the user correctly signs in")
	public void whenUserIsBeenSignUp() {
		// It's a Skeleton example
	}

	@Then("the user goes to dashboard")
	public void thenTheUserIsActivated() {
		// It's a Skeleton example
	}
}
