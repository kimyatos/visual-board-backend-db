package com.atos.visualdemand.controller;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Test;
import org.springframework.context.annotation.Import;

import com.atos.visualdemand.service.DeepBoxService;
import com.atos.visualdemand.test.ControllerEmbededDBRunner;

@Import(value = { DeepBoxController.class, DeepBoxService.class, VisualDemandExceptionHandler.class })
public class DeepBoxControllerTest extends ControllerEmbededDBRunner {

	private static final String GET_URL = "/deepbox/get";
	private static final String UPDATE_URL = "/deepbox/put";

	@Test
	public void getPreSaveDeepBox() {
		String html = doGet(GET_URL);
		assertThat(html, is(equalTo("Update Info for your Deep Box")));
	}

	@Test
	public void updateDeepBox() {
		doPostWithJsonReturnsPlainText(UPDATE_URL, "html", "Some update");
		String html = doGet(GET_URL);
		assertThat(html, is(equalTo("Some update")));
		
	}
	
}
