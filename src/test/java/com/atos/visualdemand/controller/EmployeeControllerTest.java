package com.atos.visualdemand.controller;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

import java.util.Base64;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.atos.visualdemand.model.Edge;
import com.atos.visualdemand.model.Employee;
import com.atos.visualdemand.model.Request;
import com.atos.visualdemand.repository.EdgeRepository;
import com.atos.visualdemand.repository.EmployeeRepository;
import com.atos.visualdemand.repository.RequestRepository;
import com.atos.visualdemand.service.EmployeeService;
import com.atos.visualdemand.test.ControllerEmbededDBRunner;
import com.atos.visualdemand.test.ControllerTestUtils;

@Import(value = { EmployeeController.class, EmployeeService.class, VisualDemandExceptionHandler.class })
public class EmployeeControllerTest extends ControllerEmbededDBRunner {

	private static final String SAVE_URL = "/employee/save";
	private static final String DELETE_URL = "/employee/delete";

	@Autowired
	private EmployeeRepository employeeRepository;

	@Autowired
	private EdgeRepository edgeRepository;

	@Autowired
	private RequestRepository requestRepository;

	@Test
	public void employeeIsSaved() {
		Employee employeeToSave = ControllerTestUtils.buildNotSavedValidEmployee();

		Employee savedEmployee = doPostWithJson(SAVE_URL, employeeToSave);

		assertNotNull(savedEmployee);
		assertNotNull(savedEmployee.getId());
		assertEquals(employeeToSave.getName(), savedEmployee.getName());
		assertEquals(employeeToSave.getImageUrl(), savedEmployee.getImageUrl());
		assertEquals(employeeToSave.getX(), savedEmployee.getX());
		assertEquals(employeeToSave.getY(), savedEmployee.getY());
		Assert.assertArrayEquals("Because image is not defined, will use default one",
				Base64.getDecoder().decode(EmployeeService.DEFAULT_IMAGE), savedEmployee.getImage());
	}

	@Test
	public void employeeIsNotSaved() {
		Employee employeeToSave = ControllerTestUtils.buildNotSavedValidEmployee();
		employeeToSave.setName(null);

		expectInternalServerError(SAVE_URL, employeeToSave);

	}

	@Test
	public void employeeIsDeleted() {

		Employee employee = this.employeeRepository.save(ControllerTestUtils.buildNotSavedValidEmployee());
		doDeleteById(DELETE_URL, employee.getId());
		assertThat(this.employeeRepository.findOne(employee.getId()), is(nullValue()));

	}

	@Test
	@Transactional(propagation = Propagation.NOT_SUPPORTED)
	public void employeeAndRelationsAreDeleted() {

		Employee employee = this.employeeRepository.save(ControllerTestUtils.buildNotSavedValidEmployee());
		Request request = this.requestRepository
				.save(ControllerTestUtils.buildNotSavedRequest("Request Text", 1, "Url", false));

		Edge edge = new Edge();
		edge.setSource(employee);
		edge.setTarget(request);
		edge = edgeRepository.save(edge);

		doDeleteById(DELETE_URL, employee.getId());
		assertThat(this.employeeRepository.findOne(employee.getId()), is(nullValue()));
		assertThat(this.edgeRepository.findOne(edge.getId()), is(nullValue()));
		assertThat(this.requestRepository.findOne(request.getId()), is(not(nullValue())));

	}
}
