package com.atos.visualdemand.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;

import com.atos.visualdemand.model.Employee;
import com.atos.visualdemand.model.Request;
import com.atos.visualdemand.pojo.NodesPojo;
import com.atos.visualdemand.service.EdgeService;
import com.atos.visualdemand.service.EmployeeService;
import com.atos.visualdemand.service.RequestService;
import com.atos.visualdemand.test.ControllerEmbededDBRunner;
import com.atos.visualdemand.test.ControllerTestUtils;

@Import({ BoardController.class, EmployeeService.class, RequestService.class, EdgeService.class,
		VisualDemandExceptionHandler.class })
public class BoardControllerTest extends ControllerEmbededDBRunner {

	@Autowired
	private EmployeeService employeeService;

	@Autowired
	private RequestService requestService;

	@Autowired
	private EdgeService edgeService;

	private static final String GET_ALL_URL = "/board/get";

	@Test
	public void getsAll() {
		int numRequest = 4;
		int numEmployees = 6;
		int numEdges = 2;
		List<Employee> employees = ControllerTestUtils.saveMultipleEmployee(numEmployees, employeeService);
		List<Request> requests = ControllerTestUtils.saveMultipleRequest(numRequest, requestService);
		ControllerTestUtils.saveMultipleEdges(numEdges, employees.get(0), requests.get(0), edgeService);

		NodesPojo result = doGet(GET_ALL_URL, NodesPojo.class);

		assertNotNull(result);
		assertNotNull(result.getEmployees());
		assertNotNull(result.getRequests());
		assertNotNull(result.getEdges());

		assertEquals(numRequest, result.getRequests().spliterator().getExactSizeIfKnown());
		assertEquals(numEmployees, result.getEmployees().spliterator().getExactSizeIfKnown());
		assertEquals(numEdges, result.getEdges().spliterator().getExactSizeIfKnown());
		result.getEdges().forEach((current) -> {
			assertNull(current.getSource());
			assertNull(current.getTarget());
			assertNotNull(current.getSourceId());
			assertNotNull(current.getTargetId());
		});
	}
}
