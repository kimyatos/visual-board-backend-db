package com.atos.visualdemand.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.atos.visualdemand.model.Edge;
import com.atos.visualdemand.model.Employee;
import com.atos.visualdemand.model.Request;
import com.atos.visualdemand.repository.EdgeRepository;
import com.atos.visualdemand.service.EdgeService;
import com.atos.visualdemand.service.EmployeeService;
import com.atos.visualdemand.service.RequestService;
import com.atos.visualdemand.test.ControllerEmbededDBRunner;
import com.atos.visualdemand.test.ControllerTestUtils;

@Import({ EdgeController.class, EmployeeService.class, RequestService.class, EdgeService.class,
		VisualDemandExceptionHandler.class })
public class EdgeControllerTest extends ControllerEmbededDBRunner {
	private static final String SAVE_URL = "/edge/save";
	private static final String DELETE_URL = "/edge/delete";

	@Autowired
	private EmployeeService employeeService;

	@Autowired
	private RequestService requestService;

	@Autowired
	private EdgeService edgeService;

	@Autowired
	private EdgeRepository edgeRepository;

	@Test
	public void edgeIsSaved() {
		long sourceId = ControllerTestUtils.saveMultipleEmployee(1, employeeService).get(0).getId();
		long targetId = ControllerTestUtils.saveMultipleRequest(1, requestService).get(0).getId();

		Edge savedEdge = doPostWithJson(SAVE_URL, edgeFromSourceAndTarget(sourceId, targetId));
		assertNotNull(savedEdge);
		assertNull(savedEdge.getSource());
		assertNotNull(savedEdge.getSourceId());
		assertNull(savedEdge.getTarget());
		assertNotNull(savedEdge.getTargetId());

		assertEquals(sourceId, savedEdge.getSourceId().longValue());
		assertEquals(targetId, savedEdge.getTargetId().longValue());
	}

	@Test
	public void edgeWillNotSaveBecauseSourceIsNotInDatabase() {
		long sourceId = ControllerTestUtils.saveMultipleEmployee(1, employeeService).get(0).getId();

		expectInternalServerError(SAVE_URL, edgeFromSourceAndTarget(sourceId, 1L));
	}

	@Test
	public void edgeWillNotSveBecauseTargetIsNotInDatabase() {
		long targetId = ControllerTestUtils.saveMultipleRequest(1, requestService).get(0).getId();

		expectInternalServerError(SAVE_URL, edgeFromSourceAndTarget(1L, targetId));
	}

	@Test
	public void shouldDeleteEdge() {
		Employee employee = ControllerTestUtils.saveMultipleEmployee(1, employeeService).get(0);
		Request request = ControllerTestUtils.saveMultipleRequest(1, requestService).get(0);

		Edge edge = new Edge();
		edge.setSource(employee);
		edge.setTarget(request);
		Edge savedEdge = edgeRepository.save(edge);
		Long savedEdgeId = savedEdge.getId();

		assertTrue(doGetWithParams(Boolean.class, MockMvcRequestBuilders.delete(DELETE_URL)
				.param("sourceId", employee.getId().toString()).param("targetId", request.getId().toString())));
		assertNull(edgeService.findById(savedEdgeId));

	}

	@Test
	public void shouldReportExceptionWhenEdgeDoesnotExists() {
		expectInternalServerErrorWithCustomBuilder(MockMvcRequestBuilders.get(DELETE_URL).param("edgeId", "2"));
	}

	private Edge edgeFromSourceAndTarget(Long source, Long target) {
		Edge edge = new Edge();
		edge.setSourceId(source);
		edge.setTargetId(target);
		return edge;
	}
}
