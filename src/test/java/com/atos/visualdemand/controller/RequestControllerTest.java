package com.atos.visualdemand.controller;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.atos.visualdemand.model.Edge;
import com.atos.visualdemand.model.Employee;
import com.atos.visualdemand.model.Request;
import com.atos.visualdemand.model.Technology;
import com.atos.visualdemand.repository.EdgeRepository;
import com.atos.visualdemand.repository.EmployeeRepository;
import com.atos.visualdemand.repository.RequestRepository;
import com.atos.visualdemand.service.RequestService;
import com.atos.visualdemand.test.ControllerEmbededDBRunner;
import com.atos.visualdemand.test.ControllerTestUtils;

@Import(value = { RequestController.class, RequestService.class, VisualDemandExceptionHandler.class })
public class RequestControllerTest extends ControllerEmbededDBRunner {

	private static final String HTTP_WWW_PLAN_FORMATION_SHAREPOINT = "http://www.plan.formation.sharepoint";
	private static final String SAVE_URL = "/request/save";
	private static final String DELETE_URL = "/request/delete";

	@Autowired
	private EmployeeRepository employeeRepository;

	@Autowired
	private EdgeRepository edgeRepository;

	@Autowired
	private RequestRepository requestRepository;

	@Test
	public void requestIsSaved() {
		Request requestToSave = ControllerTestUtils.buildNotSavedRequest("Text title", 5,
				HTTP_WWW_PLAN_FORMATION_SHAREPOINT, true);
		requestToSave.setTechnology(Technology.OPEN_SOURCE);

		Request savedRequest = doPostWithJson(SAVE_URL, requestToSave);

		assertThat(savedRequest.getJuniorProgrammers(), is(equalTo(5)));
		assertThat(savedRequest.getStartingDate(), is(equalTo(requestToSave.getStartingDate())));
		assertThat(savedRequest.getUrlFormationPlan(), is(equalTo(HTTP_WWW_PLAN_FORMATION_SHAREPOINT)));
		assertThat(savedRequest.isFormationPlan(), is(equalTo(true)));
		assertThat(savedRequest.getId(), is(notNullValue()));
	}

	@Test
	public void requestIsNotSaved_WhenTextIsNull() {
		Request requestToSave = ControllerTestUtils.buildNotSavedRequest(null, 5, HTTP_WWW_PLAN_FORMATION_SHAREPOINT,
				true);

		expectInternalServerError(SAVE_URL, requestToSave);
	}

	@Test
	public void requestIsDeleted() {

		Request request = this.requestRepository
				.save(ControllerTestUtils.buildNotSavedRequest("text", 1, "url", false));
		doDeleteById(DELETE_URL, request.getId());
		assertThat(this.requestRepository.findOne(request.getId()), is(nullValue()));

	}

	@Test
	@Transactional(propagation = Propagation.NOT_SUPPORTED)
	public void requestAndRelationsAreDeleted() {

		Employee employee = this.employeeRepository.save(ControllerTestUtils.buildNotSavedValidEmployee());
		Request request = this.requestRepository
				.save(ControllerTestUtils.buildNotSavedRequest("text", 1, "url", false));

		Edge edge = new Edge();
		edge.setSource(employee);
		edge.setTarget(request);
		edge = this.edgeRepository.save(edge);

		doDeleteById(DELETE_URL, request.getId());
		assertThat(this.employeeRepository.findOne(employee.getId()), is(not(nullValue())));
		assertThat(this.edgeRepository.findOne(edge.getId()), is(nullValue()));
		assertThat(this.requestRepository.findOne(request.getId()), is(nullValue()));

	}
}