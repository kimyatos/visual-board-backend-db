package com.atos.visualdemand.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Before;
import org.junit.Test;

import com.atos.visualdemand.test.ControllerEmbededDBRunner;

public class EdgeTest extends ControllerEmbededDBRunner {

	private Employee employee;
	private Request request;
	private Edge edge;

	@Before
	public void init() {
		employee = new Employee();
		employee.setId(2L);

		request = new Request();
		request.setId(1L);

		edge = new Edge();
		edge.setId(4L);
		edge.setSource(employee);
		edge.setTarget(request);
	}

	@Test
	public void shouldAssignId() {
		edge.withIdsDefined();
		assertEquals(edge.getSource().getId(), edge.getSourceId());
		assertEquals(edge.getTarget().getId(), edge.getTargetId());
	}

	@Test
	public void shouldSetSourceAndTargetAsNull() {
		edge.withNullSourceAndTarget();
		assertNull(edge.getSource());
		assertNull(edge.getTarget());
	}
}
