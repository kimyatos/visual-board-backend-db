package com.atos.visualdemand.test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.atos.visualdemand.model.Edge;
import com.atos.visualdemand.model.Employee;
import com.atos.visualdemand.model.Request;
import com.atos.visualdemand.service.EdgeService;
import com.atos.visualdemand.service.EmployeeService;
import com.atos.visualdemand.service.RequestService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Utils functions to be used in tests.
 * 
 * @author A643239
 *
 */
public class ControllerTestUtils {

	public static String asJsonString(final Object obj) {
		try {
			return new ObjectMapper().writeValueAsString(obj);
		} catch (JsonProcessingException e) {
			throw new RuntimeException(e);
		}
	}

	public static <T> T fromJsonStringToObject(final String json, Class<T> clazz) {
		try {
			return new ObjectMapper().readValue(json, clazz);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public static Request buildNotSavedRequest(String text, int juniorProgrammers, String urlFormationPlan,
			boolean formationPlan) {
		Request requestNotSaved = new Request();
		requestNotSaved.setJuniorProgrammers(juniorProgrammers);
		requestNotSaved.setText(text);
		requestNotSaved.setUrlFormationPlan(urlFormationPlan);
		requestNotSaved.setFormationPlan(formationPlan);
		requestNotSaved.setStartingDate(new Date());
		requestNotSaved.setX(1);
		requestNotSaved.setY(2);
		return requestNotSaved;
	}

	public static Employee buildNotSavedValidEmployee() {
		Employee retVal = new Employee();
		retVal.setName("Unsaved employee");
		retVal.setX(1);
		retVal.setY(2);
		return retVal;
	}

	public static List<Request> saveMultipleRequest(int numRequest, RequestService service) {
		List<Request> retVal = new ArrayList<>();
		for (int i = 0; i < numRequest; i++) {
			Request newRequest = buildNotSavedRequest("Request " + i, 1, "none", false);
			retVal.add(newRequest);
			service.saveOrUpdate(newRequest);
		}
		return retVal;
	}

	public static List<Employee> saveMultipleEmployee(int numEmployees, EmployeeService service) {
		List<Employee> retVal = new ArrayList<>();
		for (int i = 0; i < numEmployees; i++) {
			Employee newEmployee = buildNotSavedValidEmployee();
			newEmployee.setName("Employee " + i);
			retVal.add(newEmployee);
			service.saveOrUpdate(newEmployee);
		}
		return retVal;
	}

	public static List<Edge> saveMultipleEdges(int numEdges, Employee source, Request target, EdgeService service) {
		List<Edge> retVal = new ArrayList<>();
		for (int i = 0; i < numEdges; i++) {
			Edge newEdge = new Edge();
			newEdge.setSource(source);
			newEdge.setTarget(target);
			retVal.add(newEdge);
			service.saveOrUpdate(newEdge);
		}
		return retVal;
	}

	private ControllerTestUtils() throws InstantiationException {
		throw new UnsupportedOperationException();
	}
}
