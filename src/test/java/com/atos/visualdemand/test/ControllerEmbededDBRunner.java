package com.atos.visualdemand.test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.atos.visualdemand.AtosApplication;
import com.atos.visualdemand.exception.VisualDemandCommonException;
import com.atos.visualdemand.pojo.ExceptionResponse;

/**
 * <h1>Holds basic configuration for running test from the endpoint to the
 * DB.</h1>
 * <ul>
 * <li>Relies on a embedded DB.</li>
 * <li>It is necessary to Import your controller in your test class.</li>
 * </ul>
 * 
 * @author A643239
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = { AtosApplication.class })
@WebAppConfiguration
@DataJpaTest
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.HSQL, replace = Replace.ANY)
@TestPropertySource("classpath:test.properties")
public abstract class ControllerEmbededDBRunner {

	@Autowired
	private WebApplicationContext	webApplicationContext;

	public MockMvc					mockMvc;

	@Before
	public void setUp() {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
	}

	protected <R> R doGet(String url, Class<R> resultClass) {
		try {
			MvcResult resultOfRequest = mockMvc
					.perform(MockMvcRequestBuilders.get(url).contentType(MediaType.APPLICATION_JSON)).andReturn();
			return ControllerTestUtils.fromJsonStringToObject(resultOfRequest.getResponse().getContentAsString(),
					resultClass);
		} catch (Exception e) {
			throw new VisualDemandCommonException(e);
		}
	}

	protected String doGet(String url) {
		try {
			MvcResult resultOfRequest = mockMvc
					.perform(MockMvcRequestBuilders.get(url).contentType(MediaType.APPLICATION_JSON)).andReturn();
			return resultOfRequest.getResponse().getContentAsString();
		} catch (Exception e) {
			throw new VisualDemandCommonException(e);
		}
	}

	protected <R> R doGetWithParams(Class<R> resultClass, RequestBuilder builder) {
		try {
			MvcResult resultOfRequest = mockMvc.perform(builder).andReturn();
			return ControllerTestUtils.fromJsonStringToObject(resultOfRequest.getResponse().getContentAsString(),
					resultClass);
		} catch (Exception e) {
			throw new VisualDemandCommonException(e);
		}
	}

	/**
	 * Executes a POST request with a JSON contentType
	 * 
	 * @param url
	 *            target mockMvc URL
	 * @param objectToSend
	 *            object to encode as JSON
	 * @return Converted to object JSON received from server (if possible)
	 * @throws VisualDemandCommonException
	 *             when an error occurred
	 */
	@SuppressWarnings("unchecked")
	protected <E> E doPostWithJson(String url, E objectToSend) {
		try {
			MvcResult resultOfRequest = mockMvc.perform(MockMvcRequestBuilders.post(url)
					.contentType(MediaType.APPLICATION_JSON).content(ControllerTestUtils.asJsonString(objectToSend)))
					.andReturn();
			return (E) ControllerTestUtils.fromJsonStringToObject(resultOfRequest.getResponse().getContentAsString(),
					objectToSend.getClass());
		} catch (Exception e) {
			throw new VisualDemandCommonException(e);
		}
	}

	/**
	 * Executes a POST request with a JSON contentType
	 * 
	 * @param url
	 *            target mockMvc URL
	 * @param objectToSend
	 *            object to encode as JSON
	 * @param key key in request
	 * @param value of the key param.
	 * 
	 * @return Plain text
	 * @throws VisualDemandCommonException
	 *             when an error occurred
	 */
	protected <E> String doPostWithJsonReturnsPlainText(String url, String key, String value) {
		try {
			MvcResult resultOfRequest = mockMvc.perform(MockMvcRequestBuilders.post(url)
					.contentType(MediaType.APPLICATION_JSON).param(key, value))
					.andReturn();
			return resultOfRequest.getResponse().getContentAsString();
		} catch (Exception e) {
			throw new VisualDemandCommonException(e);
		}
	}

	/**
	 * Executes a DELETE request
	 * 
	 * @param url
	 *            target mockMvc URL
	 * @param id
	 *            key of object to be delete
	 * @return MvcResult
	 * @throws VisualDemandCommonException
	 *             when an error occurred
	 */
	protected MvcResult doDeleteById(String url, Long id) {
		try {
			MvcResult resultOfRequest = mockMvc.perform(MockMvcRequestBuilders.delete(url + "/" + id))
					.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			return resultOfRequest;
		} catch (Exception e) {
			throw new VisualDemandCommonException(e);
		}
	}

	protected void expectInternalServerErrorWithCustomBuilder(RequestBuilder builder) {
		try {
			MvcResult resultOfRequest = mockMvc.perform(builder)
					.andExpect(MockMvcResultMatchers.status().isInternalServerError()).andReturn();

			ExceptionResponse exceptionResponse = ControllerTestUtils.fromJsonStringToObject(
					resultOfRequest.getResponse().getContentAsString(), ExceptionResponse.class);
			assertThat(exceptionResponse.getStatus(), is(equalTo(HttpStatus.INTERNAL_SERVER_ERROR.name())));
		} catch (Exception e) {
			throw new VisualDemandCommonException(e);
		}
	}

	/**
	 * Will do an assertThat() checking if target <b>url</b> returns 500 HTTP
	 * status
	 * 
	 * @param url
	 *            target mockMvc URL
	 * @param objectToSend
	 *            object to encode as JSON
	 */
	protected void expectInternalServerError(String url, Object objectToSend) {
		try {
			MvcResult resultOfRequest = mockMvc
					.perform(MockMvcRequestBuilders.post(url).contentType(MediaType.APPLICATION_JSON)
							.content(ControllerTestUtils.asJsonString(objectToSend)))
					.andExpect(MockMvcResultMatchers.status().isInternalServerError()).andReturn();
			ExceptionResponse exceptionResponse = ControllerTestUtils.fromJsonStringToObject(
					resultOfRequest.getResponse().getContentAsString(), ExceptionResponse.class);

			assertThat(exceptionResponse.getStatus(), is(equalTo(HttpStatus.INTERNAL_SERVER_ERROR.name())));
		} catch (Exception e) {
			throw new VisualDemandCommonException(e);
		}
	}
}
