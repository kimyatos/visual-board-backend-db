##DOCKER BUILD
* Run it using security. Using maven run: _package docker:build -Dmaven.test.skip=true_
* Run it without security. Using maven run:  _package docker:build -Dmaven.test.skip=true  -Ddocker.folder=src/main/dockernosecurity_

##REMOTE DEBUG
* Add _"-agentlib:jdwp=transport=dt_socket,server=y,suspend=y,address=1044"_ to the docker file as an argument. The backend will be blocked till you connect to the socket.
